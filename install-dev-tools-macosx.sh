#!/bin/sh

# install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install Git, GCC, CMake, Clang, GCOV, LCOV, Cppcheck, Valgrind, Python (+pip+pylint+test and coverage modules+scan-build for Clang SA)

# install packages with homebrew
brew update
brew install git gcc lcov cppcheck valgrind python llvm --with-toolchain

# install some additional python modules for python analysis
pip3 install --upgrade pip
pip3 install pytest pytest-cov pylint

# get scan-build for clang static analysis
wget https://clang-analyzer.llvm.org/downloads/checker-279.tar.bz2
tar xf checker-279.tar.bz2
sudo ln -s $PWD/checker-279/bin/scan-build /usr/local/bin/scan-build

./install_drmemory_unix.sh
./install_lcov_to_cobertura_unix.sh
