#!/bin/sh

# Install drmemory
wget https://github.com/DynamoRIO/drmemory/releases/download/release_2.2.0/DrMemory-Linux-2.2.0-1.tar.gz
tar xf DrMemory-Linux-2.2.0-1.tar.gz
export PATH=$PWD/DrMemory-Linux-2.2.0-1/bin64:$PATH
