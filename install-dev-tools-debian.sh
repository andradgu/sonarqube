#!/bin/sh

# Install Git, GCC, CMake, Clang, GCOV, LCOV, Cppcheck, Valgrind, Python (+pip+pylint+test and coverage modules+scan-build for Clang SA)
sudo apt-get update -y
sudo apt-get install -y git build-essential cmake clang clang-tidy gcovr lcov cppcheck valgrind python-pip pylint

pip install --upgrade pip
sudo python -m pip install pytest pytest-cov setuptools scan-build

# Install the script to convert lcov coverage report into xml file compatible with SonarQube
wget https://github.com/eriwen/lcov-to-cobertura-xml/archive/1.6.tar.gz
tar xvf 1.6.tar.gz
sudo ln -s $PWD/lcov-to-cobertura-xml-1.6/lcov_cobertura/lcov_cobertura.py /usr/local/bin/lcov_cobertura.py

./install_drmemory_unix.sh
./install_lcov_to_cobertura_unix.sh
