#!/bin/sh

# Install Git, GCC, CMake, Clang, GCOV, LCOV, Cppcheck, Valgrind, Python (+pip+pylint+test and coverage modules+scan-build for Clang SA)
sudo yum install -y epel-release
sudo yum update -y
sudo yum install -y git gcc gcc-gfortran gcc-c++ cmake clang llvm-toolset-7-clang-tools-extra.x86_64 lcov cppcheck valgrind python python-pip pylint

sudo pip install --upgrade pip
sudo python -m pip install pytest pytest-cov setuptools scan-build

./install_drmemory_unix.sh
./install_lcov_to_cobertura_unix.sh
