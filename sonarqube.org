#+TITLE: Static analysis and SonarQube web service
#+AUTHOR: Florent Pruvost, Service Expérimentation et Dévelopement, Inria Bordeaux Sud-Ouest
#+LANGUAGE:  en
#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t _:nil ^:nil -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil pri:nil tags:not-in-toc html-style:nil
#+PROPERTY: header-args:sh :noweb no-export :session local :exports code :results none

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="css/bigblow.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="css/hideshow.css"/>
#+HTML_HEAD: <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="js/jquery-ui-1.10.2.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="js/jquery.localscroll-min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="js/jquery.scrollTo-1.4.3.1-min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="js/jquery.zclip.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="js/bigblow.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="js/hideshow.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="js/jquery.stickytableheaders.min.js"></script>


This document presents an example of source code (C/C++, Python)
analysis and publication of results to the [[https://sonarqube.inria.fr/sonarqube][Inria SonarQube web server]].

Many concepts and tools that are used here are discussed in the
[[https://sonarqube.inria.fr/pages/documentation.html][Inria SonarQube
documentation]]. Please refer to this documentation to get more
information.

The toy project used here is [[https://gitlab.inria.fr/sed-bso/heat][Heat]].

Summary of the hands-on session:
1. install prerequisites
2. test sonarqube scanner with a python "hello world"
3. download the Heat source code from the public git repository
4. build the project: generate a library, executables and a report
   containing the GCC compiler warnings, and generate clang static
   analyzer reports
5. execute tests: necessary to produce the coverage (gcov) data
6. perform analysis: gcov, cppcheck, valgrind
7. create the sonar-project.properties file and call *sonar-scanner* to
   publish the results to SonarQube
8. analyze results with the SonarQube web interface

* Generalities

  [[https://www.sonarqube.org/][SonarQube]] is mainly a web server providing some dashboards that help
  assessing the quality of programs. Many things can be reported: size
  of source code, detection of duplication, issues based on static and
  dynamic analyzers, coverage data, etc.

  There exists different versions released: one is opensource, free
  but with limited features and some others are charged, see [[https://www.sonarsource.com/plans-and-pricing/][Plans &
  Pricing]]. [[https://about.sonarcloud.io/][SonarCloud]] is a freely available instance of SonarQube for
  opensource projects. However it is charged for private projects.

  After some experiments with the opensource *Community Edition* version
  we have concluded that it meets requirements for most Inria usage.
  We host an instance on a virtual machine managed by the [[https://intranet.inria.fr/Inria/Directions/Systemes-d-information/DSI/Direction][DSI]] and the
  [[https://intranet.inria.fr/Inria/Directions/Transfert-partenariats-industriels/DGDT/SED][SED]] of Inria. The url of the service is :
  [[https://sonarqube.inria.fr/sonarqube][https://sonarqube.inria.fr/sonarqube]]. This is a *Community Edition
  LTS*.  Here is the list of plugins installed: [[https://sonarqube.inria.fr/pages/installed-plugins.html][Installed Plugins]]. To
  get more [[https://docs.sonarqube.org/display/PLUG/Plugin+Library][plugins]] installed please contact the [[mailto:sed-sonarqube.helpdesk-prc@helpdesk.inria.fr][support
  team]]. Languages that can be analyzed are: C, C++, C#, CSS, Flex, Go,
  Groovy, Lua, Java, JavaScript, Kotlin, PHP, Python, Ruby, Scala,
  TypeScript, XML, see [[https://www.sonarqube.org/features/multi-languages/][list of supported languages]].

  Without being logged only public projects are displayed. Any user is
  able to explore metrics and source code of public projects. This
  platform allows people with an Inria account to analyze their
  project. People with no Inria account will be able to join existing
  projects. Once logged users can:
  * create a new project (only Inria account), being automatically
    administrator, through a [[https://docs.sonarqube.org/display/SCAN][sonar-scanner analysis]]
  * join (browse metrics, see source code, administer issues, etc)
    existing project if proper permissions are given by one of the
    project's administrators

* Initial setup
  In the following we will perform analysis of source codes locally on
  a personal computer and then publish the results on the
  SonarQube@inria web server. This requires to have some tools
  installed and an internet access.

  Considering the experiment environment we propose either to download a
  ready to use [[https://docs.docker.com/engine/docker-overview/][Docker]] image or to install the analysis tools in your
  own environment on your personal computer.

** Use the ready to use Docker image
   First if not already done, [[https://docs.docker.com/install/][install Docker]].

   Then one can download the existing Docker image
   #+begin_src sh
   docker pull hpclib/sonarqube
   #+end_src
   Notice this image has been made with this [[https://gitlab.inria.fr/sed-bso/sonarqube/blob/master/Dockerfile][Dockerfile]].

   Finally run interactively in the container with
   #+begin_src sh
   docker run -it hpclib/sonarqube
   #+end_src

   In this environment all the analysis tools used in the following
   are available, sonar-scanner, coverity, cppcheck, clang, gcov,
   valgrind, etc. Thus you don't need to follow the "installation"
   recommandation given below. Still, the token creation is required,
   see next section.

** Create a SonarQube token
   To be able to publish results to SonarQube a token system is
   necessary for the authentication.  Log in to
   https://sonarqube.inria.fr/sonarqube with your Inria LDAP
   credential and create a [[https://sonarqube.inria.fr/pages/documentation.html#org693e559][personal token]] (save it in your home for
   example in ~$HOME/.sonarqubetoken~)

** Install sonar-scanner
   If you don't use the Docker image you will need to install
   sonar-scanner to perform a static analysis, index source code
   files, and publish the results. Skip this stage if you use the
   Docker image.

   Please install the [[https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner][sonar-scanner program]].

   We provide some installation scripts for some well known operating
   systems, please refer to one of the ~install-sonar-scanner-*.sh~
   scripts. wget, unzip, sudo, ln commands are required.

*** Linux
    #+begin_src sh :eval no :tangle install-sonar-scanner-linux.sh :shebang "#!/bin/sh" :export none
    wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.0.0.1744-linux.zip
    unzip sonar-scanner-cli-4.0.0.1744-linux.zip
    sudo ln -s $PWD/sonar-scanner-4.0.0.1744-linux/bin/sonar-scanner /usr/local/bin/sonar-scanner
    #+end_src
*** MacOSX
    #+begin_src sh :eval no :tangle install-sonar-scanner-macosx.sh :shebang "#!/bin/sh" :export none
    wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.0.0.1744-macosx.zip
    unzip sonar-scanner-cli-4.0.0.1744-macosx.zip
    sudo ln -s $PWD/sonar-scanner-4.0.0.1744-macosx/bin/sonar-scanner /usr/local/bin/sonar-scanner
    #+end_src
*** Windows
    On *Windows*, download the appropriate [[https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/][archive]], extract the archive
    and add its sub-directory "/bin" in the *PATH* environment variable
    (Start Menu -> Right clic on Computer -> Properties -> Advanced
    system settings -> Environment Variables -> Path -> Edit).
** Hello World SonarQube
   Lets try sonar-scanner with an "Hello World" python script.

   First create the "hello world" python script
   #+begin_src python :tangle hello_world.py
   # This program prints Hello, world!

   print('Hello, world!')
   #+end_src
   you can execute it if you have python installed
   #+begin_src sh
   python hello_world.py
   #+end_src

   Optionnaly save your token in a file, for example
   #+begin_src sh
   echo "34539dd39d77f225f552676191a23ff725116702" > ~/.sonarqubetoken
   #+end_src

   Then lets use sonar-scanner to analyze this source code
   #+begin_src sh
   sonar-scanner -D"sonar.host.url=https://sonarqube.inria.fr/sonarqube" -D"sonar.login=${cat ~/.sonarqubetoken}" -D"sonar.projectKey=sedbso:helloworld:test" -D"sonar.sources=./hello_word.py" -D"sonar.language=py"
   #+end_src

   sonar-scanner is used in a shell through a CLI. One can pass the
   options directly in the command line or they can be stored in a
   file ~sonar-project.properties~.

   #+begin_src sh :tangle sonar-project.properties
   sonar.host.url=https://sonarqube.inria.fr/sonarqube
   sonar.login=34539dd39d77f225f552676191a23ff725116702
   sonar.projectKey=sedbso:helloworld:test
   sonar.sources=./hello_world.py
   sonar.language=py
   #+end_src

   Then you should be able to invoke sonar-scanner more simply
   #+begin_src sh
   sonar-scanner
   #+end_src

   Note that one can set some options in the file and change their
   values at the level of the command line (take precedence).

** Install some development and analysis tools for C/C++ and Python programs
   Please skip this stage if you use the Docker image.

   heat is a toy code written in C that will be used in this hands-on
   session to perform static and dynamic analysis.

   We propose to install some dependencies of the /heat/ program, git, C
   compiler (GCC), CMake, plus some useful analysis tools, Clang
   Static Analyzer, Cppcheck, GCOV, Valgrind, pytest, pylint ...

   We provide some scripts to do that depending on the OS, see
   ~install-dev-tools-*.sh~ scripts.

*** Debian (apt-get)
    #+begin_src sh :eval no :tangle install-dev-tools-debian.sh :shebang "#!/bin/sh" :export none
    # Install Git, GCC, CMake, Clang, GCOV, LCOV, Cppcheck, Valgrind, Python (+pip+pylint+test and coverage modules+scan-build for Clang SA)
    sudo apt-get update -y
    sudo apt-get install -y git build-essential cmake clang clang-tidy gcovr lcov cppcheck valgrind python-pip pylint

    pip install --upgrade pip
    sudo python -m pip install pytest pytest-cov setuptools scan-build

    # Install the script to convert lcov coverage report into xml file compatible with SonarQube
    wget https://github.com/eriwen/lcov-to-cobertura-xml/archive/1.6.tar.gz
    tar xvf 1.6.tar.gz
    sudo ln -s $PWD/lcov-to-cobertura-xml-1.6/lcov_cobertura/lcov_cobertura.py /usr/local/bin/lcov_cobertura.py

    ./install_drmemory_unix.sh
    ./install_lcov_to_cobertura_unix.sh
    #+end_src
*** Centos (yum)
    #+begin_src sh :eval no :tangle install-dev-tools-centos.sh :shebang "#!/bin/sh" :export none
    # Install Git, GCC, CMake, Clang, GCOV, LCOV, Cppcheck, Valgrind, Python (+pip+pylint+test and coverage modules+scan-build for Clang SA)
    sudo yum install -y epel-release
    sudo yum update -y
    sudo yum install -y git gcc gcc-gfortran gcc-c++ cmake clang llvm-toolset-7-clang-tools-extra.x86_64 lcov cppcheck valgrind python python-pip pylint

    sudo pip install --upgrade pip
    sudo python -m pip install pytest pytest-cov setuptools scan-build

    ./install_drmemory_unix.sh
    ./install_lcov_to_cobertura_unix.sh
    #+end_src
*** Fedora (dnf)
    #+begin_src sh :eval no :tangle install-dev-tools-fedora.sh :shebang "#!/bin/sh" :export none
    # Install Git, GCC, CMake, Clang, GCOV, LCOV, Cppcheck, Valgrind, Python (+pip+pylint+test and coverage modules+scan-build for Clang SA)
    sudo dnf upgrade -y
    sudo dnf install -y git gcc gcc-gfortran gcc-c++ cmake clang clang-tools-extra gcovr lcov cppcheck valgrind python python-pip pylint

    sudo pip install --upgrade pip
    sudo python -m pip install pytest pytest-cov setuptools scan-build

    ./install_drmemory_unix.sh
    ./install_lcov_to_cobertura_unix.sh
    #+end_src
*** MacOSX
    #+begin_src sh :eval no :tangle install-dev-tools-macosx.sh :shebang "#!/bin/sh" :export none
    # install Homebrew
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    # Install Git, GCC, CMake, Clang, GCOV, LCOV, Cppcheck, Valgrind, Python (+pip+pylint+test and coverage modules+scan-build for Clang SA)

    # install packages with homebrew
    brew update
    brew install git gcc lcov cppcheck valgrind python llvm --with-toolchain

    # install some additional python modules for python analysis
    pip3 install --upgrade pip
    pip3 install pytest pytest-cov pylint

    # get scan-build for clang static analysis
    wget https://clang-analyzer.llvm.org/downloads/checker-279.tar.bz2
    tar xf checker-279.tar.bz2
    sudo ln -s $PWD/checker-279/bin/scan-build /usr/local/bin/scan-build

    ./install_drmemory_unix.sh
    ./install_lcov_to_cobertura_unix.sh
    #+end_src
*** Windows
    * Install Git, see https://git-scm.com/download/win, add the Git
      command in the path, /e.g./
      #+begin_src
      "C:\Program Files\Git\cmd"
      #+end_src
    * Install CMake, see https://cmake.org/install/, add cmake to the
      path, /e.g./
      #+begin_src
      "C:\Program Files\CMake\bin"
      #+end_src
    * Install MinGW, see http://www.mingw.org/wiki/MSYS, with gcc, g++
      gfortran, and whatever you think may be useful, add MinGW and
      MSYS in the path, /e.g./
      #+begin_src
      "C:\MinGW\bin";"C:\MinGW\msys\1.0\bin"
      #+end_src
    * Install Python, see https://www.python.org/downloads/windows/, add
      python to the path, /e.g./
      #+begin_src
      "C:\Users\ci\AppData\Local\Programs\Python\Python37"
      #+end_src
    * Install Cppcheck, see http://cppcheck.sourceforge.net/
    * Install Clang, see http://clang.llvm.org/get_started.html#build
    * Install Dr Memory, see https://drmemory.org/
* Scan a program
** Download Heat with git
   In a shell, git clone the *Heat* source code, this is a public git
   repository
   #+name: download
   #+begin_src sh
   # git command is required
   git clone https://gitlab.inria.fr/sed-bso/heat.git
   cd heat/
   #+end_src

** Build
   Configure and build Heat so that:
   * we enable and save some chosen GCC warnings
   * we enable coverage
   * we perform a static analysis with Clang SA

   #+name: install
   #+begin_src sh
   mkdir -p build
   cd build
   # make clean
   CFLAGS="--coverage -fPIC -fdiagnostics-show-option -Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wmissing-prototypes -Wstrict-prototypes -Wcomment -pedantic -g"
   LDFLAGS="--coverage"
   scan-build -v -plist --intercept-first --analyze-headers -o analyzer_reports cmake .. -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_C_FLAGS="$CFLAGS" -DCMAKE_EXE_LINKER_FLAGS="$LDFLAGS"
   scan-build -v -plist --intercept-first --analyze-headers -o analyzer_reports make 2>&1 | tee heat-build.log
   #+end_src

   Remarks:
   - *CMAKE_VERBOSE_MAKEFILE* is set to ON to see compilation lines
   - *CMAKE_EXPORT_COMPILE_COMMANDS* is set to ON to get compile
     commands used during the make process. It will be used later for
     the clang-tidy analysis and cppcheck
   - "--coverage" is used for coverage analysis with *gcov*, see
     https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html#Instrumentation-Options
   - "-fPIC" should be used if a shared library is built
   - "-fdiagnostics-show-option" is used to feed GCC warnings into
     SonarQube, see
     https://github.com/SonarOpenCommunity/sonar-cxx/wiki/Compilers
   - "-Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare
     -Wmissing-prototypes -Wstrict-prototypes -Wcomment -pedantic" are
     used to enable some GCC warnings (arbitrarily)
   - *scan-build* program is used to perform a clang static analyzer
     scan, see https://clang-analyzer.llvm.org/scan-build.html.
   - we build with debug information "-g" to help *scan-build* (clang
     analyzer) to be more effective (avoid false positive), see
     https://clang-analyzer.llvm.org/scan-build.html#recommended_debug
   - scan-build results are stored in ~analyzer_reports~
   - "-plist" option is used for getting some report files compatible
     with SonarQube
** Test
   We perform unitary tests and this generates gcov coverage files
   thanks to the "--coverage" flags used just before, see resulting
   /*.gcda/ and /*.gcno/ files
   #+name: tests
   #+begin_src sh
   ctest -V
   #+end_src

   #+begin_src sh
   find . -regex '.*\.\(gcda\|gcno\)'
   #+end_src
** Perform static and dynamic analysis
   SonarQube performs a static analysis when we invoke *sonar-scanner*
   (see the next section "SonarQube scan"). The issues that are
   considered by the SonarQube analysis depends on the language and for
   C/C++ it relies on the [[https://github.com/SonarOpenCommunity/sonar-cxx][sonar-cxx plugin]]. In addition to its own
   analysis the plugin allows to import issues addressed by external
   analysis tools.

   In this section we run some of the external tools that are
   compatible to perform static (clang-tidy, cppcheck) and
   dynamic analysis (coverage with gcov, valgrind, drmemory).

   Notice we already have performed the clang static analysis during
   the build step. There is no need to do something else, the clang
   reports generated with the "-plist" parameter are compatible with
   SonarQube.

*** clang
    The [[http://clang.llvm.org/][Clang]] project provides two tools for static analysis, [[https://clang-analyzer.llvm.org/][clang-sa]]
    and [[https://clang.llvm.org/extra/clang-tidy/][clang-tidy]]. clang-sa covers a variety of checks targeted at
    finding security and API usage bugs, dead code, and other logic
    errors, see [[https://clang-analyzer.llvm.org/available_checks.html][available checkers]]. clang-tidy is more a "linter" tool
    for diagnosing and fixing typical programming errors, like style
    violations, interface misuse, etc.

    clang-sa has been used during the build process, please see the
    previous section about the build. Report files should have been
    generated in the ~build/analyzer_reports/~ directory and are ready
    to be imported in SonarQube.

    clang-tidy can be invoked to add code smells reports.
    #+name: clangtidy
    #+begin_src
    # in the build directory
    clang-tidy -checks='*' -header-filter=.. -p . ../*.c > clang-tidy-report
    # or even better using the compilation database and 5 threads
    /usr/lib/llvm-9/share/clang/run-clang-tidy.py -checks='*' -header-filter=.. -p . -j5 > clang-tidy-report
    #+end_src

    See also
    https://baptiste-wicht.com/posts/2017/03/clang-tidy-static-analysis-integration-in-sonarqube.html.

    SonarQube list of Clang issues for C code :
    * [[https://sonarqube.inria.fr/sonarqube/coding_rules#languages=c|repositories=ClangSA-c][clang-sa]]
    * [[https://sonarqube.inria.fr/sonarqube/coding_rules#languages=c|repositories=ClangTidy-c][clang-tidy]]

*** cppcheck
    See
    https://sonarqube.inria.fr/pages/documentation.html#org4575413.
    #+name: cppcheck
    #+begin_src sh
    cd ..
    DEFINITIONS=""
    CPPCHECK_INCLUDES="-I."
    SOURCES_TO_EXCLUDE="-ibuild/CMakeFiles/"
    SOURCES_TO_ANALYZE="."
    cppcheck -v -f --language=c --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${DEFINITIONS} ${CPPCHECK_INCLUDES} ${SOURCES_TO_EXCLUDE} ${SOURCES_TO_ANALYZE} 2> heat-cppcheck.xml
    # windows "C:\Program Files\Cppcheck\cppcheck.exe" -v -f --language=c --platform=win64 --enable=all --xml --xml-version=2 -I. -ibuild\CMakeFiles . 2> heat-cppcheck.xml
    #+end_src
    Remarks:
    - "-f" to force maximum number of configurations (combination of
      all macros states), see "--max-configs" option to
      limit the number of configurations because it can be very long
      or "-D" and "-U" to define/undefine macros
    - "--enable=all" enables all error types checking
    - "--xml" allows to generate a report compatible with SonarQube

    SonarQube list of Cppcheck issues for C code : [[https://sonarqube.inria.fr/sonarqube/coding_rules#languages=c|repositories=cppcheck-c][cppcheck]]
*** valgrind
    See
    https://sonarqube.inria.fr/pages/documentation.html#orge2639b3.
    [[http://valgrind.org/][Valgrind]] is used to detect memory management and threading bugs.
    #+name: valgrind
    #+begin_src sh
    valgrind --xml=yes --xml-file=heat-valgrind.xml --memcheck:leak-check=full --show-reachable=yes "./build/heat_seq" "10" "10" "200" "0" "0"
    #+end_src

    SonarQube list of Valgrind issues for C code : [[https://sonarqube.inria.fr/sonarqube/coding_rules#languages=c|repositories=valgrind-c][valgrind]]
*** drmemory
    See https://drmemory.org/
    #+name: drmemory
    #+begin_src sh
    mkdir drmemory
    $DRMEMORY/drmemory -logdir drmemory -- build/heat_seq "10" "10" "200" "0" "0"
    # windows "C:\Program Files (x86)\Dr. Memory\bin64\drmemory.exe" -logdir C:\Users\ci\heat\drmemory -- build\heat_seq.exe "10" "10" "200" "0" "0"
    #+end_src

    SonarQube list of DrMemory issues for C code : [[https://sonarqube.inria.fr/sonarqube/coding_rules#languages=c|repositories=drmemory-c][drmemory]]
*** coverage with GCOV and LCOV
    After the tests execution you can generate the coverage
    report. You can already generate an HTML file using *lcov* and
    *genhtml*.  *lcov_cobertura.py* script is necessary to produce the xml
    report necessary to publish coverage on SonarQube (the .lcov
    report cannot be imported as it is).
    #+name: coverage
    #+begin_src sh
    lcov --directory . --capture --output-file heat.lcov
    genhtml -o coverage heat.lcov
    # see the result, for example: firefox coverage/index.html
    lcov_cobertura.py heat.lcov --output heat-coverage.xml
    #+end_src
    See
    https://sonarqube.inria.fr/pages/documentation.html#org9e9e84d
    for more information.

    Actually it could also be possible to import the unitary tests if
    the test framework supports "JUnitReport-like" such as *Googletest*
    and *Boost Test*, see
    - https://github.com/SonarOpenCommunity/sonar-cxx/wiki/Get-test-execution-metrics
    - https://github.com/SonarOpenCommunity/sonar-cxx/wiki/Test-runners
*** And more
    Other reports can be imported such as :
    * the compiler warnings as we have discussed in the previous
      section "Build", see
      https://github.com/SonarOpenCommunity/sonar-cxx/wiki/Compilers
    * PC-lint (not free)
    * Vera++, see
      https://sonarqube.inria.fr/pages/documentation.html#orgec582f5
    * RATS, see
      https://security.web.cern.ch/security/recommendations/en/codetools/rats.shtml
** SonarQube scan from the command line
   SonarQube performs a static analysis when we invoke
   *sonar-scanner*. The issues that are considered by the SonarQube
   analysis depends on the language and for C/C++ it relies on the
   [[https://github.com/SonarOpenCommunity/sonar-cxx][sonar-cxx plugin]].

   The *sonar-scanner* program is used for executing the SonarQube scan
   and put the results online /i.e./ upload to the [[https://sonarqube.inria.fr/sonarqube][server]]. The program
   takes some arguments such as the url of the server, credentials,
   the name we want to assign to our project analysis, paths to
   external analysis reports, etc.

   See [[https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner#AnalyzingwithSonarQubeScanner-Use][how to use it]].  Parameters can be given either with the -D
   argument of the program and/or defining them in a configuration
   file ~sonar-project.properties~ which must be located in the current
   path where sonar-scanner is invoked.

*** config scanner
    For our use case we define project's specific parameters in the
    local configuration file, *please adapt the projectkey* parameter
    and replace with your name /e.g./ fpruvost -> hmathieu
    #+name: scanconfig
    #+begin_src sh :tangle sonar-project.properties
    sonar.links.homepage=https://gitlab.inria.fr/sed-bso/heat
    sonar.links.scm=https://gitlab.inria.fr/sed-bso/heat.git
    sonar.projectKey=sedbso:heat:fpruvost
    sonar.projectDescription=Solve the heat propagation equation
    sonar.projectVersion=1.0
    sonar.scm.disabled=false
    sonar.scm.provider=git
    sonar.sourceEncoding=UTF-8
    sonar.sources=.
    sonar.exclusions=build/CMakeFiles/**
    sonar.language=c
    sonar.c.errorRecoveryEnabled=true
    sonar.c.compiler.parser=GCC
    sonar.c.includeDirectories=$(echo | gcc -E -Wp,-v - 2>&1 | grep "^ " | tr '\n')
    sonar.c.compiler.charset=UTF-8
    sonar.c.compiler.regex=^(.*):(\\\d+):\\\d+: warning: (.*)\\\[(.*)\\\]$
    sonar.c.compiler.reportPath=build/heat-build.log
    sonar.c.clangsa.reportPath=build/analyzer_reports/*/*.plist
    sonar.c.clangtidy.reportPath=build/clang-tidy-report
    sonar.c.coverage.reportPath=heat-coverage.xml
    sonar.c.cppcheck.reportPath=heat-cppcheck.xml
    sonar.c.valgrind.reportPath=heat-valgrind.xml
    sonar.c.drmemory.reportPath=drmemory/**/results.txt
    #+end_src
    Remarks:
    - for explanation about the main sonar-scanner parameters please
      read
      - https://sonarqube.inria.fr/pages/documentation.html#org8a50dfe
      - https://docs.sonarqube.org/display/SONAR/Analysis+Parameters
    - please follow the convention we described [[https://sonarqube.inria.fr/pages/documentation.html#orgda0f182][here]] about the
      *sonar.projectKey* (/e.g./ team:project:git:branch) parameter, and
      please do not provide the *sonar.projectName* (projectKey will be
      used as name), it helps to keep projects organized in SonarQube
    - the *sonar.c* parameters are related to the [[https://github.com/SonarOpenCommunity/sonar-cxx/wiki][sonar-cxx]] plugin and
      allows to complete the analysis with reports coming from other
      tools. In this example we import issues coming from
      - GCC warnings: compiler.reportPath
      - Clang-SA: clangsa.reportPath
      - Clang-tidy: clangtidy.reportPath
      - GCOV: coverage.reportPath
      - Cppcheck: cppcheck.reportPath
      - Valgrind: valgrind.reportPath
      - DrMemory: drmemory.reportPath

*** sonar-scanner
    We invoke sonar-scanner with -X (debug), the address of the
    sonarqube server, the personal token for identification, and we
    save the output in a file
    #+name: scanner
    #+begin_src sh
    sonar-scanner -X -Dsonar.host.url=https://sonarqube.inria.fr/sonarqube -Dsonar.login=`cat ~/.sonarqubetoken` >sonar.log 2>&1
    #+end_src
    The ~.sonarqubetoken~ file must contain your personal token in this
    example. You need to give your personal token here to be able to
    upload some data, see the first section "Initial setup".

    See the results in [[https://sonarqube.inria.fr/sonarqube/][SonarQube]]. In the "Projects" tab search for
    your project in the research bar (the projectKey parameter).

    Note that SonarQube analysis can be performed differently than
    using the sonar-scanner program in a shell for project building
    with MSBuild, Maven, Gradle, Ant, Jenkins, see [[https://docs.sonarqube.org/display/SONARQUBE67/Analyzing+Source+Code][here]].

** Scan a C++ program
   The same tools as for C program can be used. The only things to
   change are some parameters in the file ~sonar-project.properties~.
   - sonar.c properties should be replaced by sonar.cxx
   - sonar.language should be set to "c++".

** Scan a Python program
   Example with the [[https://github.com/flowersteam/explauto][Explauto]] library.

   Install
   #+begin_src sh
   git clone https://github.com/flowersteam/explauto.git
   cd explauto
   python3 -m pip install codecov pytest-cov setuptools
   python3 -m pip install -r requirements.txt
   python3 setup.py build
   sudo python3 setup.py develop
   #+end_src

   Test + Coverage
   #+begin_src sh
   pytest --cov=./ --junit-xml=test.xml --cov-report xml
   #+end_src

   Setup the SonarQube configuration
   #+begin_src sh :tangle sonar-project.properties
   sonar.links.homepage=https://githutb.com/flowersteam/explauto
   sonar.links.scm=https://github.com/flowersteam/explauto.git
   sonar.projectKey=sedbso:explauto:fpruvost
   sonar.projectDescription=An autonomous exploration library
   sonar.projectVersion=1.4.0
   sonar.scm.disabled=false
   sonar.scm.provider=git
   sonar.sourceEncoding=UTF-8
   sonar.sources=.
   sonar.exclusions=build
   sonar.language=py
   sonar.python.xunit.reportPath=test.xml
   sonar.python.coverage.reportPaths=coverage.xml
   #+end_src

   Perform sonarqube analysis and publish the results
   #+begin_src sh
   sonar-scanner -X -Dsonar.host.url=https://sonarqube.inria.fr/sonarqube -Dsonar.login=`cat ~/.sonarqubetoken` >sonar.log 2>&1
   #+end_src
   Notice SonarQube perfoms also a pylint analysis if it is installed
   on the system. In addition unitary tests and coverage are imported
   thanks to the xml files generated before with pytest.

   See the results in [[https://sonarqube.inria.fr/sonarqube/dashboard?id=flowers%3Aexplauto%3Atest][SonarQube]].

** SonarQube public server for opensource softwares
   The C/C++ analyzer from SonarQube is a paid service, see
   [[https://www.sonarsource.com/products/codeanalyzers/sonarcfamilyforcpp.html][SonarCFamily plugin]]. This plugin cannot be used in the community
   version we have instanciated at Inria. For C/C++ we use the
   community plugin [[https://github.com/SonarOpenCommunity/sonar-cxx/wiki][sonar-cxx]]. With this plugin we get access to a few
   SonarQube issues (during the sonar-scanner analysis) and we can
   import many other reports coming from external analyzers. This is
   fine but we don't have access to the "official" SonarQube C/C++
   analyzer which is surely interesting too.

   The good news ? There exists a public SonarQube server, [[https://sonarcloud.io/about][sonarcloud]]
   for opensource projects which gives access to the SonarCFamily
   plugin.

   If you have an opensource project to analyze and you want to see
   what gives the sonar-scanner analysis with the SonarCFamily plugin
   please visit the [[https://sonarcloud.io/documentation][sonarsource documentation]].

   See for example the [[https://sonarcloud.io/dashboard?id=simgrid_simgrid][SimGrid project]].
** The Coverity alternative
   Coverity is another analyzer that is normally a paid service but
   which gives access to a public server for opensource projects,
   see :
   * [[https://scan.coverity.com/][Main page]]
   * [[https://scan.coverity.com/users/sign_in][Dashboard]]

   Download the cov-build program [[https://scan.coverity.com/download][here]].
   #+begin_src sh
   COVERITY_TOKEN="XEJaJ1cAnqW-9M_zkmxd7w"
   wget --no-check-certificate https://scan.coverity.com/download/linux64 --post-data "token=$COVERITY_TOKEN&project=Heat" -O coverity_tool.tgz
   tar xvf coverity_tool.tgz
   sudo ln -s -f $PWD/cov-analysis-linux64-*/bin/cov-build /usr/local/bin/cov-build
   #+end_src

   Build and analyze with the 'cov-build' command
   #+begin_src sh
   git clone https://gitlab.inria.fr/sed-bso/heat.git
   cd heat/
   mkdir -p build
   cd build
   cmake ..
   make clean
   cov-build --dir cov-int make -j 4
   #+end_src

   Create a compress tar archive of the results
   #+begin_src sh
   tar czvf heat.tgz cov-int
   #+end_src

   Submit compressed file created in previous step for analysis
   #+begin_src sh
   curl --form token=$COVERITY_TOKEN --form email=florent.pruvost@inria.fr --form file=@heat.tgz --form version="`git rev-parse --short HEAD`" --form description="" https://scan.coverity.com/builds?project=Heat
   #+end_src

   See the results in the [[https://scan.coverity.com/projects/heat?tab=overview][Coverity dashboard]], follow the "View
   Defects" link.

   Features
   * Paid service if not opensource
   * Languages: C/C++, C#, Java, Javascript, PHP, Python, .NET Core,
     ASP.NET, Objective-C, Go, JSP, Ruby, Swift, Fortran, Scala,
     VB.NET, iOS, TypeScript
   * Well integrated in IDEs, supports most of compilers
   * Fine detection of bugs (static analysis)
   * Concurrency issues (concurrent data access, deadlocks, race conditions)
   * Security issues, ...

* SonarQube web interface
  Official [[https://docs.sonarqube.org/display/SONARQUBE67/Documentation][SonarQube documentation]].
** Overview
   Please look at the [[https://docs.sonarqube.org/display/SONARQUBE67/Project+Page][Overview]] tab of your project.

   One first thing you should think of is the question of the project
   visibility. Is your program free, opensource ? Do you want to share
   the SonarQube measures with your community ? If yes then go to the
   Administration -> Permissions tab and turn the project
   "Public". Hence the project will be examinable by anyone.

   The main metrics are displayed such as the number of Bugs
   (reliability), Vulnerabilities (security aspects), Code Smells
   (encourage to follow standards), Unitary tests and Coverage,
   Duplications. Each measure is clickable, one can explore
   - graphs of measures to get nice views of the least/most impacted
     files
     - circles at top-left : this is not good you should do something
     - circles at bottom-right : good job
   - activity: historical trends in the course of several analysis
   - lists of issues

   The Quality Gate (QG), is always in a "Passed" (green) status
   because we have decided not to use it by default. One can use it to
   define a state change of the code quality. For example if we get
   new bugs between two analysis then the QG becomes red and
   developers should be notified and fix the bugs to turn the QG
   green.

   There is also the notion of "New Bugs", code smells etc, since
   previous version. What does this mean ? It simply reports how
   things get worse since the previous version number, the number used
   for the /sonar.projectVersion/ parameter. If you change the version
   from 1.0 to 2.0 subsequent analysis will report new issues since
   the last analysis of version 1 in the "Leak Period: since 1.0"
   yellow part.
** Issues
   By clicking on [[https://docs.sonarqube.org/display/SONARQUBE67/Issues][Issues]] you've got the list of all issues (bugs,
   vulnerabilities, code smells). The left part of the interface
   allows to filter the issues to improve the navigation and
   understanding.

   This Issues tab is convenient to explore the results of static and
   dynamic analysis coming from the different tools (sonar-scanner,
   warning gcc, clang-sa, clang-tidy, cppcheck, valgrind, drmemory,
   etc) and to manage them. By manage we mean to decide whether or not
   the issues should be fixed or not. By default new issues have an
   /Open/ status. The goal is to decrease the number of open issues. The
   first thing to do in the interface is to choose which issues should
   be fixed in a near future or not.
   1) Issues to be fixed are tagged /Confirmed/. If in the next analysis
      issues are not in the reports anymore then SonarQube will turn
      them as /Fixed/ and /Closed/.
   2) Issues that are not fixed should be classified. Why not fixing
      these issues ? Is it because
      A. this is a false positive, and the issue should be tagged
         /false positive/
      B. the issue is known and we accept to keep it as it is for good
         reasons, the issue should be tagged /won't fix/

   Remark: closed issues are kept for 30 days in the database.

   There are many way to filter issues, see on the left side of the
   tab
   - Type : bug, vulnerabilities, code smells
   - Resolution and Status : Unresolved, fixed , Open, Closed, ...
   - Rule : the issue name
   - Directory, File
   - Assignee, Author
   - Language
** Measures
   The [[https://docs.sonarqube.org/display/SONARQUBE67/Metric+Definitions][Measures]] tab allows to get synthetic views with graphs,
   treemap, directory tree, about the bugs, vulnerabilities, code
   smells, coverage, unitary tests, duplications, size, complexity,
   issue kinds.
** Code
   The [[https://docs.sonarqube.org/display/SONARQUBE67/Code+Viewer][Code]] tab allows to navigate in the code structure directory by
   directory and get the size (lines of code), the number of issues,
   the coverage percentage and the duplications.
** Activity
   The [[https://docs.sonarqube.org/display/SONARQUBE67/Activity+and+History][Activity]] tab informs about the history of analysis. One can
   find the dates and some graphs to follow the trend of some
   measures.
** Administration
   See
   https://docs.sonarqube.org/display/SONARQUBE67/Project+Administration+Guide.
*** Quality Profiles
    You can change the rules applied to your project, see
    https://docs.sonarqube.org/display/SONARQUBE67/Project+Settings#ProjectSettings-QualityGateandSettingQualityProfiles.
*** Permissions
    You can update permissions to make the project public or private
    (to hide or not the source code), add and remove users to be able
    to administrate the project and its issues. See also
    https://sonarqube.inria.fr/pages/faq.html#faq1.
** Bonus
*** Email notifications
    You can register to email notifications that inform about new
    analysis results and give a link to the project new issues.

    Go to "My Account" (top right corner) -> "Notifications", in
    "Notifications per project", write the name of your project in the
    search bar, select it then check the boxes.
*** Bagdes
    [[https://github.com/QualInsight/qualinsight-plugins-sonarqube-badges/][SVG badges]] can be added in some web pages in order to show the code
    quality measures.
*** 3D view
    See the tab *More* -> SoftVis3D Viewer.
