#!/bin/sh

wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.0.0.1744-macosx.zip
unzip sonar-scanner-cli-4.0.0.1744-macosx.zip
sudo ln -s $PWD/sonar-scanner-4.0.0.1744-macosx/bin/sonar-scanner /usr/local/bin/sonar-scanner
