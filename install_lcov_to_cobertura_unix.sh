#!/bin/sh

# Install the script to convert lcov coverage report into xml file compatible with SonarQube
wget https://github.com/eriwen/lcov-to-cobertura-xml/archive/1.6.tar.gz
tar xvf 1.6.tar.gz
sudo ln -s $PWD/lcov-to-cobertura-xml-1.6/lcov_cobertura/lcov_cobertura.py /usr/local/bin/lcov_cobertura.py
